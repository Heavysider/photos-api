# Dependencies
1. PostgreSQL
2. minio file storage
3. exiftool

## Minio
As you suggested I used minio as a file storage. I used it through docker.
To start minio with docker you need:

1. Docker :D
2. `docker pull minio/minio`
3. `docker run -p 9000:9000 -e "MINIO_SECRET_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY" -e "MINIO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLE" minio/minio server /data`

This should start minio server instance with those "example" secret and acces keys.

## Exiftool

In order to change metadata of images I used ***mini_exiftool*** gem, that depends on the exiftool library. In order to install library:

 - On Linux: [https://linoxide.com/linux-how-to/install-use-exiftool-linux-ubuntu-centos/](https://linoxide.com/linux-how-to/install-use-exiftool-linux-ubuntu-centos/)
 - On Mac Os: `brew install exiftool`

## Magic

`./bin/setup` - This should setup everything - gems, create databases, run migrations

# API CALLS
I used ***Postman*** during my tests. All params should be sent as ***form-data*** in the

**POST** `/api/v1/users`  - Creates new user
Params:
-	user[name] - required
-	user[email] - required
-	user[password] - required
-	user[password_confirmation] - required
---
**POST** `/auth/login`  - Creates new user
Params:
-	email - required
-	password - required

This will return a JWT ***token***, which you will need to provide to all of the api requests to authorize them. In order to do that, you will need to choose Bearer Token as your type of Authorization and paste the received token.

---
**POST** `/api/v1/photos`  - Uploads new photo
Params:
-	photo[image] - required - your uploaded file
---
**PATCH** `/api/v1/photos/:id`  - Updates image's metadata
Params:
-	metadata[...] - required. You can as well update multiple EXIF tags in one request. EXIF tags should be compatible with exiftool. All compatible tags are listed here: [https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html](https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html)
---
**POST** `/api/v1/albums`  - Creates new Album
Params:
-	album[name] - required
---

**PATCH**`/photos/:id/add_to_albums`

- albums[ids][] - array of albums ids where user wants to add their images

---

Other than that all CRUD works for ***photos*** and ***albums***

# Regarding Testing

I wrote some really simple tests for models, I have no idea how to right test for controllers (I didn't have any prior testing experience) yet and it really differs from company to company and from project to project, in my opinion, so if you will teach me how to do that for your specific project and for your team - I will gladly do that :)

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :photos
      resources :albums
      resources :users, param: :_username
      post '/auth/login', to: 'authentication#login'
      get '/photos/:id/download', to: 'photos#download', as: :download
      get '/photos/:id/get_metadata', to: 'photos#get_metadata'
      patch '/photos/:id/add_to_albums', to: 'photos#add_to_albums'
    end
  end
end

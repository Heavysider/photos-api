require 'rails_helper'

RSpec.describe Album, type: :model do

  it 'should be instantiable' do
    expect { album = Album.new }.not_to raise_error
  end

  it 'should return error if name is absent' do
    album = Album.create
    expect(album.errors.messages[:name].length).to eq 1
    expect(album.errors.messages[:name].first).to eq("can't be blank")
  end

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:photos) }
end

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'should be instantiable' do
    expect { user = User.new }.not_to raise_error
  end

  it 'should return error if name is absent' do
    user = User.create(email: 'test@test.com', password: 'testtest', password_confirmation: 'testtest')
    expect(user.errors.messages[:name].length).to eq 1
    expect(user.errors.messages[:name].first).to eq("can't be blank")
  end

  it 'should return error if email is absent' do
    user = User.create(name: 'Test Name', password: 'testtest', password_confirmation: 'testtest')
    expect(user.errors.messages[:email].length).to eq 2
  end

  it 'should return error if email is duplicated' do
    User.create(name: 'Test Name', email: 'test_duplicate@test.com', password: 'testtest', password_confirmation: 'testtest')
    user = User.create(name: 'Test Name', email: 'test_duplicate@test.com', password: 'testtest', password_confirmation: 'testtest')
    expect(user.errors.messages[:email].length).to eq 1
    expect(user.errors.messages[:email].first).to eq("has already been taken")
  end

  it 'should return error if password is empty' do
    user = User.create(name: 'Test Name', email: 'test@test.com', password_confirmation: 'testtest')
    expect(user.errors.messages[:password].length).to eq 2
  end

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:email) }
  it { is_expected.to respond_to(:photos) }
  it { is_expected.to respond_to(:albums) }
  it { is_expected.to respond_to(:password_digest) }
end

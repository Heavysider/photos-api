require 'rails_helper'

RSpec.describe Photo, type: :model do

  it 'should be instantiable' do
    expect { photo = Photo.new }.not_to raise_error
  end

  it { is_expected.to respond_to(:albums) }
end

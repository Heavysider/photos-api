class RemoveUrlAndNameFromPhotos < ActiveRecord::Migration[5.2]
  def change
    remove_column :photos, :url
    remove_column :photos, :name
  end
end

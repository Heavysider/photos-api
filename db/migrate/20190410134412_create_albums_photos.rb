class CreateAlbumsPhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.string :name
      t.belongs_to :user
      t.timestamps
    end

    create_table :photos do |t|
      t.string :url
      t.belongs_to :user
      t.timestamps
    end

    create_table :albums_photos, id: false do |t|
      t.belongs_to :album, index: true
      t.belongs_to :photo, index: true
    end
  end
end

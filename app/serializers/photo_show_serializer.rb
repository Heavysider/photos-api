class PhotoShowSerializer < PhotoSerializer
  meta do |object|
    object.get_metadata
  end
end

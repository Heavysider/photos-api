class PhotoSerializer
  include FastJsonapi::ObjectSerializer
  set_type :photo
  set_id :id
  belongs_to :user
  has_many :albums

  link :self do |object|
    Rails.application.routes.url_helpers.api_v1_photo_url(object.id)
  end

  link :download do |object|
    Rails.application.routes.url_helpers.api_v1_download_url(object.id)
  end

  attribute :filename do |object|
    object.image.filename.to_s
  end

  attribute :url do |object|
    Rails.application.routes.url_helpers.url_for(object.image)
  end

end

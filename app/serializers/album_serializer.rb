class AlbumSerializer
  include FastJsonapi::ObjectSerializer
  set_type :album
  set_id :id
  attribute :name
  belongs_to :user
  has_many :photos

  link :self do |object|
    Rails.application.routes.url_helpers.api_v1_album_url(object.id)
  end
end

class Api::V1::AlbumsController < ApplicationController
  before_action :authorize_request
  before_action :find_album, except: [:index, :create]

  def index
    render json: ::AlbumSerializer.new(@current_user.albums).serialized_json, status: :ok
  end

  def create
    album = Album.create(album_params)
    album.user_id = @current_user.id
    if album.save
      redirect_to api_v1_album_url(album)
    else
      render json: { status: 422, errors: album.errors.full_messages }, status: :unprocessible_entity
    end
  end

  def show
    render json: AlbumSerializer.new(@album)
  end

  def update
    if @album.update_attributes(album_params)
      redirect_to api_v1_album_url(@album)
    else
      render json: { status: 422, errors: @album.errors.full_messages }, status: :unprocessible_entity
    end
  end

  def destroy
    @album.destroy
    if @album.destroy
      redirect_to api_v1_albums_url
    else
      render json: { status: 422, errors: @album.errors.full_messages }, status: :unprocessible_entity
    end

  end

  private

  def find_album
    begin
      @album = @current_user.albums.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { status: 404, errors: ["Couldn't find an Album"] }, status: :not_found
    end
  end

  def album_params
    params.require(:album).permit(:name)
  end
end

class Api::V1::PhotosController < ApplicationController
  before_action :authorize_request
  before_action :find_photo, except: [:index, :create]

  def index
    render json: ::PhotoSerializer.new(@current_user.photos).serialized_json, status: :ok
  end

  def create
    photo = Photo.new(photo_params)
    photo.user_id = @current_user.id
    if photo.save
      redirect_to api_v1_photo_url(photo)
    else
      render json: { status: 422, errors: photo.errors.full_messages }, status: :unprocessible_entity
    end
  end

  def download
    response.headers["Content-Type"] = @photo.image.content_type
    response.headers["Content-Disposition"] = "attachment; #{@photo.image.filename.parameters}"

    @photo.image.download { |chunk| response.stream.write(chunk) }
  end

  def show
    render json: PhotoShowSerializer.new(@photo)
  end

  def update
    if @photo.set_metadata(params[:metadata])
      redirect_to api_v1_photo_url(@photo)
    else
      render json: { status: 422, errors: @photo.errors.full_messages }, status: :unprocessible_entity
    end
  end

  def add_to_albums
    render(json: { status: 422, errors: ['Required params are not present: albums[ids][]'] }, status: :unprocessible_entity) and return if params[:albums].blank? || params[:albums][:ids].blank?
    @current_user.albums.where(id: params[:albums][:ids]).each { |album| @photo.albums << album unless @photo.albums.include?(album) }
    if @photo.save
      redirect_to api_v1_photo_url(@photo)
    else
      render json: { status: 422, errors: @photo.errors.full_messages }, status: :unprocessible_entity
    end
  end

  def destroy
    if @photo.destroy
      redirect_to api_v1_photos_url
    else
      render json: { status: 422, errors: @photo.errors.full_messages }, status: :unprocessible_entity
    end
  end

  private

  def find_photo
    begin
      @photo = @current_user.photos.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { status: 404, errors: ["Couldn't find a Photo"] }, status: :not_found
    end
  end

  def photo_params
    params.require(:photo).permit(:name, :url, :image)
  end

end

class User < ApplicationRecord
  has_secure_password

  has_many :photos
  has_many :albums
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password,
            length: { minimum: 6 },
            presence: true,
            if: -> { new_record? || !password.nil? }
end

class Photo < ApplicationRecord
  has_and_belongs_to_many :albums

  has_one_attached :image

  def get_metadata
    tempfile = Tempfile.open([ "ActiveStorage", self.image.blob.filename.extension_with_delimiter ], Dir.tmpdir)

    begin
      # Downloading the image in the temp file
      copy_image_in_tempfile(tempfile)

      # Reading adn returning the metadata
      MiniExiftool.new tempfile
    ensure
      tempfile.close!
    end

  end

  def set_metadata(metadata_hash)
    tempfile = Tempfile.open([ "ActiveStorage", self.image.blob.filename.extension_with_delimiter ], Dir.tmpdir)

    begin
      # Downloading an image into the tempfile
      copy_image_in_tempfile(tempfile)

      # Editing the metadata of the image
      photo = MiniExiftool.new(tempfile.path)
      metadata_hash.each do |k, v|
        photo.send("#{k}=", v)
      end
      photo.save

      # Preparing filename and content_type for the new file
      filename = self.image.filename.to_s
      content_type = self.image.content_type

      # Purging the original image
      self.image.purge

      # Replacing it with the new updated image
      self.image.attach(io: tempfile, filename: filename, content_type: content_type)
    ensure
      tempfile.close!
      tempfile.unlink
    end
    self.save
  end

  private

  def copy_image_in_tempfile(tempfile)
    tempfile.binmode
    self.image.download { |chunk| tempfile.write(chunk) }
    tempfile.flush
    tempfile.rewind
  end
end
